# Project 2 - Using flask to create a simple webserver.

Author: Ahmed Al Ali

This is a simple project that allows the simple server to return status depending on the person's request.

* It should return a template with a success code 200 if the data exists.

* Returns error 404 template if the requested template does not exist.

* Returns error 403 if one of the illegal characters have been used (// or ~ or ..)

* Returns error 401 if the link has been added without .html or .css (NOT IMPLEMENTED ERROR)

* Note: All outcomes have been tested and were successful with the tests.sh provided in Project 1.