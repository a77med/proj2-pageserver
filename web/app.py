# Author: Ahmed Al Ali

from flask import Flask, render_template, request
import os

STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"

app = Flask(__name__)


@app.route("/", defaults={'path': ''})
@app.route("/<path:path>")
def index(path):
    # This runs in the case of homepage, or default path.
    if path == "": 
        return "Welcome to CIS 322!",200

    # Returns error 403 if there are illegal characters.
    if ((("~") in path) or (("..") in path) or (("//") in path)):
        return error_403()
    # If it ends with .html or .css get into the statement.
    elif path.endswith(".html") or path.endswith(".css"):
        # double-check the illegal characters
        if ((("~") in path) or (("..") in path) or (("//") in path)):
            return error_403()
        # No illegal characters
        else:
            # Try pulling and sending the template
            try:
                return (render_template(path),200)
            # Return error 404 if it was not found.
            except:
                return error_404()

    else:
        return error_401()


#Functions for error handling
@app.errorhandler(404)
def error_404():
    return render_template("404.html"),404

@app.errorhandler(403)
def error_403():
    return render_template("403.html"),403

@app.errorhandler(401)
def error_401():
    return render_template("401.html"),401

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
